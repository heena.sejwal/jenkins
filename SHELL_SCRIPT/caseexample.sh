#! /usr/bin/bash

echo -e " Enter some character : \c"
read value

case $value in
	[a-z] )
		echo "User entered $value is small letter" ;;
	[A-Z] )
		echo "User entered $value is capital letter" ;;
	[0-9] )
		echo "User entered $value is numeric value" ;;
	? )
		echo "User entered $value is special character" ;;
	* )
		echo " unknown input" ;;
	esac
